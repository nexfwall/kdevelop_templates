# README #

This repository contains project, and file templates suitable to use with **KDevelop** application.

### How to use ###

First you need to pack them up in a *tar.bz2* archive. **packtemplate.sh** can help you with that.

Also this repository contains a **CMakeLists.txt** file, so you can create this archives using CMake. **CMakeLists.txt** was written according to instructions on [official techbase wiki](https://techbase.kde.org/Development/Tutorials/KDevelop/Creating_a_project_template#With_CMake). But this will require you to install **KDevPlatform** and **KDE4** development packages, containing necessary CMake macros.

After obtaining template archives, just go to **KDevelop**->**KDevelop settings**->**Templates**, select tab according to a type of template(*file* or *project* template), click **Install from file** button and locate archive file you want to install.

### Contribution guidelines ###

You can contribute your own templates with pull requests, if you want. I would glad to see what people is interested in this. But there is the rules.

*   All *project* templates go to **Project** directory. All *file* templates go to **Files** directory. You can find them in the root of this repository.
*   *Project* template name must be in this format: `[build_manager]_[language_name]`
     *   There `[build_manager]` is a build manager used to build this project with. Plain Makefile(make), autoconf/automake(automake), or CMake(cmake).
     *   There `[language_name]` is a language name. If this template assumes usage of Toolkit, Framework, or Language Extensions(if those), short name of those must be prefixed before a [language_name].
  
          Examples are: **QtN** is **qtN**, **KDE Framework N** is **kfN**(usage of KDE Frameworks already involves the usage of **Qt**)(There is no need in **KDE Framework** templates, they are available by default after installing *kdesdk* package. This is just an example), **GTKN** is **gtkN**, **GObject** is **gobj**, if no frameworks/toolkits are used, the prefix is **plain**.

*   *File* template name: **[Coming soon]**

Language corrections and translations in .kdevtemplate files are also welcome.

### License ###

This project is covered under **MIT** license. As simple, and **Open Source** license, compatible with GPL. You can read your rights and **Copyright** notice in **LICENSE** file in the root of this repository.

### Contact ###

* By all questions you can send message to @nexfwall.