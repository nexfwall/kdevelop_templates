#!/bin/bash
# packtemplate.sh - pack template into an archive.
# 
# @param $1       - path to template directory.
# @return         - nothing usable. Output of /usr/bin/tar.
#
# This script expects a path to template directory as first argument.
# Example: packtemplate.sh Projects/make_plainc
# 
# Results into an tar.bz2 archive in current directory.
# Named as "${TEMPLATE_DIR_BASENAME}.tar.bz2".
OPWD="${PWD}"
cd "${1}"
tar --exclude ".gitignore" -v -cjf "${OPWD}/$(basename "${1}").tar.bz2" ./*